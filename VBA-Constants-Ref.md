VBA.Constants

# Built-in Constants

Built-in constants are sometimes referred to as intrinsic constants.


|VBA.Constants|Chr|Comments|
|-------------|---|--------|
|vbCr         |Chr(13)|Carriage return character|
|vbLf	|Chr(10)|	Linefeed character|
|vbCrLf|Chr(13) + Chr(10)|Carriage return - linefeed combination|
|vbNewLine|Chr(13) + Chr(10)|New line character|
|vbNullChar|Chr(0)|Character having a value of 0.|
|vbNullString|String having value 0|Not the same as a zero-length string (""); used for calling external procedures. Cannot be passed to any DLL's|
|vbTab|Chr(9)|Tab character|
|vbBack|Chr(8)|Backspace character|
|vbFormFeed|Chr(12)|Word VBA Manual - manual page break |
|vbVerticalTab|Chr(11)|Word VBA Manual - manual line break (Shift + Enter)|
|vbObjectError|-2147221504 (&H80040000)|Constant indicating error is being returned from a Visual Basic object|