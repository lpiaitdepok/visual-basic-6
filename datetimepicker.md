DateTimePicker properties:

* Name
* CausesValidation
* Container
* DataField
* DataFormat
* DataMember
* DataSource
* DragIcon
* DragMode
* Enabled
* Font
* Height
* HelpContextID
* hWnd
* Index
* Left
* MouseIcon
* MousePointer
* Object
* OLEDropMode
* Parent
* TabIndex
* TabStop
* Tag
* ToolTipText
* Top
* Visible
* WhatsThisHelpID
* Width
* CalendarBackColor
* CalendarForeColor
* CalendarTitleBackColor
* CalendarTitleForeColor
* CalendarTrailingForeColor
* CheckBox
* CustomFormat
* DataBindings
* DataChanged
* DateOfWeek
* Day
* Format
* Hour
* MaxDate
* MinDate
* Minute
* Month
* Second
* UpDown
* Value
* Year


```vb
' make DTPicker blank
DTPicker1.CustomFormat = " "
```