Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Const LB_SETSEL = &H185

Private Sub Command1_Click()
    If List1.SelCount Then
        SendMessage List1.hwnd, LB_SETSEL, False, ByVal True
    End If
End Sub

Private Sub Command2_Click()
    SendMessage List1.hwnd, LB_SETSEL, True, ByVal True
End Sub

Private Sub Form_Load()
    'populate listbox
    Dim i As Long
    Me.Show
    List1.Visible = False
    Me.Refresh
    For i = 1 To 10000
        List1.AddItem i
    Next
    List1.Visible = True
End Sub