# Jenis-jenis event keyboard visual basic 6.0
Di dalam visual basic 6.0 terdapat tiga (3) buah jenis event keyboard

    Event KeyPress - terjadi jika tombol-tombol yang mempunyai kode ASCII (American Standard Code for Information Interchange) pada keyboard ditekan. ASCII adalah kode dari sekumpulan karakter pada keyboard yang terdiri dari Abjad, Angka, dan Karakter khusus (Enter, Space, Tabe, Backspace)
    Event KeyDown - terjadi jika sebuah tombol keyboard ditekan
    Event KeyUp - terjadi jika sebuah tombol keyboard dilepas

Nah mungkin ada yang bertanya, apa perbedaan antara event KeyPress dan KeyUp yang keduanya akan terjadi jika ditekan tombol keyboard-nya. Berikut ini adalah jawabannya,

    Event KeyPress tidak dapat bekerja atau merespon penekanan tombol yang dikombinasi dengan Shift, Ctrl dan Alt
    Event KeyPress hanya berlaku untuk tombol-tombol yang mempunyai kode ASCII saja, tombol-tombol tertentu seperti tombol panah, tombol keypad, dan tombol fungsi (F1 s/d F12) tidak mempunyai kode ASCII

Jika event KeyPress terjadi maka event tersebut akan mengembalikan nilai argument KeyASCII yaitu kode ASCII dari tombol keyboard yang ditekan