* Intrinsic Data Control / DAO
Data1.DatabaseName = path & "\database_name"

* Remote Data Object / RDO Data Control
** Setting Up an ODBC Data Source (ODBC) / System DSN
** Project -> Components menu), select Microsoft RemoteData Control 6.0 (SP3) 
DataSourceName = DSN name
or
* Remote Data Object / RDO references object
** Project -> References menu, check Microsoft Remote Data Object 2.0
** add this following code:
```visual basic
Dim mobjRDOConn As rdoConnection

Dim mobjRDORst  As rdoResultset

Dim mstrSQL     As String
' Connect to the Property database:

Set mobjRDOConn = rdoEngine.rdoEnvironments(0).OpenConnection _

("dsn_name", rdDriverNoPrompt, , "UID=admin;PWD=")

'select or reload the data to be displayed:

mstrSQL = "select * from table_name"

Set mobjRDORst = mobjRDOConn.OpenResultset(mstrSQL, rdOpenKeyset, rdConcurRowVer)

txtCurrentQuery.Text = mstrSQL

'copy the data from the resultset to the text boxes:

txtPropNo.Text = mobjRDORst.rdoColumns("propno")

txtEmpNo.Text = mobjRDORst.rdoColumns("empno")

txtAddress.Text = mobjRDORst.rdoColumns("address")

txtCity.Text = mobjRDORst.rdoColumns("city")

txtState.Text = mobjRDORst.rdoColumns("state")

txtZip.Text = mobjRDORst.rdoColumns("zip")
```

* ADO Data Control (ADODC)
Project -> Components menu), select Microsoft ADO Data Control 6.0 (SPx)
* ADO Data  (ADODB)
Project -> References menu, check Microsoft ActiveX Data Objects 2.x