ConnectionString MySql : 
```
connstr = "Provider=MSDASQL.1;Persist Security Info=False;Data Source=contohdb"
```

ConnectionString MS Access : 
```
connstr = "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\contohdb.mdb"
```

Penggunaan Excel sebagai database dengan ConnectionString :
```
connstr = "PROVIDER=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\contohdb.xls;" & "Extended Properties=Excel 8.0;"
```

ConnectionString pada MS Sql Server : 
```
connstr = "Provider=SQLOLEDB.1;Password=aa;Persist Security Info=True;User ID=sa;Initial Catalog=contohdb;Data Source=localhost"
```

ConnectionString untuk database Oracle :
```
connstr = "Provider=MSDAORA.1;Password=aa;User ID=sys;Data Source=contohdb;Persist Security Info=False"
```

ConnectionString untuk database SQLite dengan password :
```
Data Source=c:\mydb.db;Version=3;Password=myPassword;
```

ConnectionString untuk database DBF / FoxPro :
```
Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\folder; Extended Properties=dBASE IV;User ID=Admin;Password=;
```

ConnectionString untuk database DBF / Microsoft dBASE ODBC Driver :
```
Driver={Microsoft dBASE Driver (*.dbf)};DriverID=277;Dbq=c:\mydbpath;
```

ConnectionString untuk database Firebird :
```
User=SYSDBA;Password=masterkey;Database=SampleDatabase.fdb;DataSource=localhost;
Port=3050;Dialect=3;Charset=NONE;Role=;Connection lifetime=15;Pooling=true;
MinPoolSize=0;MaxPoolSize=50;Packet Size=8192;ServerType=0;
```

ConnectionString untuk database IBM OLE DB Provider for DB2 :
```
Provider=IBMDADB2;Database=myDataBase;Hostname=myServerAddress;Protocol=TCPIP;
Port=50000;Uid=myUsername;Pwd=myPassword;
```